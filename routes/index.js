var express = require('express');
var router = express.Router();
var functions = require('../database/functions');

router.get('/', function(req, res, next) {
  res.send({ message: "OK" });
});

router.get('/lista', (req, res) => {
  functions.getAll("users").then((resultado) => {
    res.json(resultado);
  }).catch((error) => {
    res.status(500).send("Erro: " + error);
  });
});

module.exports = router;