Para inicializar o back-end, bastar utilizar o comando `npm start` no terminal e acessar o front-end para exibição dos dados. Caso queira acessar as requisições no próprio back-end, a URL é `http://localhost:3000/`. Dessa forma, qualquer alteração realizada em tempo de execução, não será aplicada, será necessário parar o projeto e iniciá-lo novamente.

Pode também ser executado com `nodemon start`. Dessa forma, as alterações são aplicadas em tempo de execução porque o servidor é inicializado automaticamente quando elas ocorrem.

Destaque dos arquivos:
- data.json: Alterei esse arquivo, mantendo-o na forma ideal: O json contém dois objetos (users e opportunities) e eles possuem um array com todos seus objetos. Para isso, tive que fazer algumas outras alterações estruturais.
- index.js: Neste arquivo, eu utilizei a propriedade getAll de functions.js para me retornar a lista de todos os elementos a partir do Promisse.

Implementado:
- A listagem de clientes foi implementada, conforme dito acima.

Não implementados:
- Visualização de cliente, com suas informações de lista de oportunidades.
- Permitir alteração do status de oportunidades.